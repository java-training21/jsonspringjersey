package com.example.JSONSpringJersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonSpringJerseyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonSpringJerseyApplication.class, args);

		//CurrenciesFromKursyWalut currenciesFromKursyWalut = new CurrenciesFromKursyWalut();
		//currenciesFromKursyWalut.ApplicationMethod();
	}

}

// INFO:
//Aplikacja napisana w Springu, wykorzystjąc Framework Jersey do parsowania API
//Aplikacja wystawia API na porcie local host 8080
//Aplikacja ma połaczenie z baza danych MySQL, szczegóły w application.properties (wymaga to uruchomienia bazy na lokalnym komputerze - MySQL installer community)
	//weryfikacja załączenia bazy -> windows->run-> services.msc -> MySQL (po kazdym restarcie komputeraz baza jest wyłączana)
//Mozna insertować do bazy z poziomu kodu (klasa Start, metoda runMethod)
//Można insertować w bazie oraz Postmanem


//Dependencies:
//jackson-databind (wymagane było usunięcie wersji: <version>2.9.4</version>)
//mysql
//lombok
//jersey-client
//spring-boot-starter-jersey
//spring-boot-starter-web
//spring-boot-starter-data-jpa
//Uruchomienie z klasy JsonSpringJerseyApplication @SpringBootApplication

