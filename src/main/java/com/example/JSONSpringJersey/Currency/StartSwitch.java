package com.example.JSONSpringJersey.Currency;


public class StartSwitch {

    //Switch   https://www.baeldung.com/java-switch

    public int exampleOfSwitch(String waluta) {

        //Scanner scanner1 = new Scanner(System.in);
        //System.out.println("Give the currency code (for example USD): ");
        //String temp = scanner1.nextLine();
        //String waluta = "";

        int result = 0;
        switch (waluta) {
            case "THB":
                result = 0;
                break;
            case "USD":
                result = 1;
                break;
            case "AUD":
                result = 2;
                break;
            case "HKD":
                result = 3;
                break;
            case "CAD":
                result = 4;
                break;
            case "NZD":
                result = 5;
                break;
            case "SGD":
                result = 6;
                break;
            case "EUR":
                result = 7;
                break;
            case "HUF":
                result = 8;
                break;
            case "CHF":
                result = 9;
                break;
            case "GBP":
                result = 10;
                break;
            case "UAH":
                result = 11;
                break;
            case "JPY":
                result = 12;
                break;
            case "CZK":
                result = 13;
                break;
            case "DKK":
                result = 14;
                break;
            case "ISK":
                result = 15;
                break;
            case "NOK":
                result = 16;
                break;
            case "SEK":
                result = 17;
                break;
            case "RON":
                result = 18;
                break;
            case "BGN":
                result = 19;
                break;
            case "TRY":
                result = 20;
                break;
            case "ILS":
                result = 21;
                break;
            case "CLP":
                result = 22;
                break;
            case "PHP":
                result = 23;
                break;
            case "MXN":
                result = 24;
                break;
            case "ZAR":
                result = 25;
                break;
            case "BRL":
                result = 26;
                break;
            case "MYR":
                result = 27;
                break;
            case "IDR":
                result = 28;
                break;
            case "INR":
                result = 29;
                break;
            case "KRW":
                result = 30;
                break;
            case "CNY":
                result = 31;
                break;
            case "XDR":
                result = 32;
                break;

            default:
                result = 7;
                break;
        }
        return result;

    }


}
