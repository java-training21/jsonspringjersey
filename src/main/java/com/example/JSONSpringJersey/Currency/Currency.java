package com.example.JSONSpringJersey.Currency;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Configuration;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Configuration
@Entity
@ToString
@Getter
@Setter
public class Currency {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//IDENTITY inkrementacja o 1, AUTO-doesn't work. With the generation GenerationType.AUTO hibernate will look for the default hibernate_sequence table , so change generation to IDENTITY
    private Long id;

    private String currencyname;
    private String code;
    private double rate;
    private String effectivedate;

    public Currency() {
    }

    public Currency(String currencyname, String code, double rate, String effectivedate) {
        this.currencyname = currencyname;
        this.code = code;
        this.rate = rate;
        this.effectivedate = effectivedate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyname() {
        return currencyname;
    }

    public void setCurrencyname(String currencyname) {
        this.currencyname = currencyname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(String effectivedate) {
        this.effectivedate = effectivedate;
    }
}
