package com.example.JSONSpringJersey.Currency;

import com.example.JSONSpringJersey.Currency.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
}