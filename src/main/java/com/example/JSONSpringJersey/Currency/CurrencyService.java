package com.example.JSONSpringJersey.Currency;

import com.example.JSONSpringJersey.Currency.Currency;
import com.example.JSONSpringJersey.Currency.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
//@Transactional
public class CurrencyService {
    @Autowired
    private CurrencyRepository currencyRepository;

    public List<Currency> listAllCurrency() {
        return (List<Currency>) currencyRepository.findAll();
    }

    public void saveCurrency(Currency currency) {
        currencyRepository.save(currency);
    }

    public Currency getCurrency(Long id) {
        return currencyRepository.findById(id).get();
    }

    public void deleteCurrency(Long id) {
        currencyRepository.deleteById(id);
    }
}
