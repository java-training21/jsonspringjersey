package com.example.JSONSpringJersey.Currency;

import com.example.JSONSpringJersey.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CurrencyController {

    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    CurrencyService currencyService;

    @PostMapping("/currencies")
    public void add(@RequestBody Currency currency) {
        currencyService.saveCurrency(currency);
    }
    public Currency createNewCurrency(@RequestBody Currency currency) {  //@Valaid przed @ReequestBody
        return currencyRepository.save(currency);
    }

    @GetMapping("/currencies/{id}")
    public ResponseEntity<Currency> getEmployeeById(@PathVariable(value = "id") Long currencyId)
            throws ResourceNotFoundException {
        Currency currency = currencyRepository.findById(currencyId)
                .orElseThrow(() -> new ResourceNotFoundException("Currency not found for id : " + currencyId));
        return ResponseEntity.ok().body(currency);
    }


    @GetMapping("/currency")
    public List<Currency> getAllCurrencies() {
        return currencyRepository.findAll();
    }
//nie dziala z CrudRepository



}
