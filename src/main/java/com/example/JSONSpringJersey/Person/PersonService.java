package com.example.JSONSpringJersey.Person;


import com.example.JSONSpringJersey.Person.Person;
import com.example.JSONSpringJersey.Person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
//@Transactional
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public List<Person> listAllPerson() {
        return (List<Person>) personRepository.findAll();
    }

    public void savePerson(Person person) {
        personRepository.save(person);
    }

    public Person getUser(Long id) {
        return personRepository.findById(id).get();
    }

    public void deletePerson(Long id) {
        personRepository.deleteById(id);
    }
}
