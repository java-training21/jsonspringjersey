package com.example.JSONSpringJersey.Person;

import com.example.JSONSpringJersey.ResourceNotFoundException;
import com.example.JSONSpringJersey.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    PersonService personService;

    @PostMapping("/people")
    public void add(@RequestBody Person person) {
        personService.savePerson(person);
    }
    public Person createNewPerson(@RequestBody Person person) {  //@Valaid przed @ReequestBody
        return personRepository.save(person);
    }

    @GetMapping("/people/{id}") //TO JEST OK
    public ResponseEntity<Person> getEmployeeById(@PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person not found for id : " + personId));
        return ResponseEntity.ok().body(person);
    }


    @GetMapping("/people")
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @PutMapping("/people/{id}")
    public ResponseEntity<Person> updatePerson(@PathVariable(value = "id") Long personId, @Valid @RequestBody Person personData) throws ResourceNotFoundException {
        Person person = findPersonById(personId);
        person.setName(personData.getName());
        person.setSurname(personData.getSurname());
        person.setAge(personData.getAge());
        return ResponseEntity.ok(personRepository.save(person));
    }


    @DeleteMapping("/people/{id}")
    public Map<String, Boolean> deletePerson(@PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person = findPersonById(personId);
        personRepository.delete(person);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private Person findPersonById(Long personId) throws ResourceNotFoundException {
        return personRepository.findById(personId).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + personId));
    }
}